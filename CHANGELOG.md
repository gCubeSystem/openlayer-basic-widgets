This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "openlayer-basic-widgets"



## [v1.4.0] - 2021-10-06

### Features

- Updated dependencies



## [v1.3.0] - 2019-04-01

### Features

- Added location and zoom support [#11708]
- Added coordinates EPSG:4326 and EPSG:3857 support [#11710]



## [v1.2.0] - 2017-06-12

### Features

- Support Java 8 compatibility [#8471]



## [v1.1.0] - 2016-12-01

### Features

- Updated to support EPSG 4326



## [v1.0.0] - 2016-10-01

### Features

- First release



